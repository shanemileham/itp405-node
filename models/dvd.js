/*jslint node: true, nomen: true, white: true*/

var Sequelize = require("sequelize");
var env       = process.env.NODE_ENV || "development";
var config    = require(__dirname + '/../config/config.json')[env];
var sequelize = new Sequelize(config.database, config.username, config.password, config);

module.exports = function (sequelize, DataTypes) {
    'use strict';
    var Dvd = sequelize.define('dvd', {
        title: {
            field: "title",
            type: Sequelize.STRING
        },
        award: {
            field: "award",
            type: Sequelize.STRING
        }
    },{
        timestamps: false
    });
    
    return Dvd;
};