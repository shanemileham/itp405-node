/*jslint node: true, sloppy: true, white: true*/
var models  = require('../models');
var express = require('express');
var router = express.Router();

var async = require('async');


router.get('/', function(req, res, next){
    res.render('index', { title: 'Express' });
});

router.get('/dvds', function(req, res, next) {
    async.parallel([
        function(callback){
            console.log(1);
        },
        function(callback){
            console.log(2);
        }
    ], function (err, results) {
        
    });
    
    
    console.log("Query Title: " + req.query.title);
    console.log("Award Title: " + req.query.award);
    
    console.time("id");
    models.dvd.findAll({
        where: {
            title: {
                like: '%'+req.query.title+'%'
            },
            award: {
                like: '%'+req.query.award+'%'
            }
        }
    }).then(function(dvds){
        console.log(dvds);
        res.render('dvds', {
            title: "Results",
            dvds: dvds
        });
        console.timeEnd("id");
    });
});

module.exports = router;
